package baseframework;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ACME_Login {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://acme-test.uipath.com/account/login");
		driver.findElementById("email").clear();
		driver.findElementById("email").sendKeys("lokeshbstest@gmail.com");
		driver.findElementById("password").clear();
		driver.findElementById("password").sendKeys("69242424");
		driver.findElementById("buttonLogin").click();
		WebElement vendorEle = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
		Actions act = new Actions(driver);
		act.moveToElement(vendorEle).build().perform();
		driver.findElementByLinkText("Search for Vendor").click();
		driver.findElementById("vendorTaxID").sendKeys("FR065748");
		driver.findElementById("buttonSearch").click();
		WebElement vendorNameEle = driver.findElementByXPath("//table[@class='table']/tbody/tr[2]/td[1]");
		String vendorName = vendorNameEle.getText();
		System.out.println(vendorName);
		/*WebElement TableName = driver.findElementByXPath("//table[@class='table']");
		List<WebElement> tableRows = TableName.findElements(By.tagName("tr"));
		System.out.println(tableRows.size());
		for(int i=0; i<=tableRows.size(); i++) {
			WebElement eachRow = tableRows.get(i);
			List<WebElement> tableColumns = eachRow.findElements(By.tagName("td"));
			 String vendorName = tableColumns.get(0).getText();
		
		*/
		if(vendorName.contentEquals("Softix Providers")) {
			System.out.println("Vendor name matches");
		}
		else {
			System.out.println("Vendor name doesn't matches");
		}
		}

	

}
