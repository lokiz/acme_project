package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC_001CheckVenorName extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC001";
		testDescription = "Verifying the Vendor Name";
		testNodes = "Ui path";
		author = "Lokesh";
		category = "smoke";
		// dataSheetName = "TC001";
	}

	@Test
	public void verifyVendorName() {

		new LoginPage()
        .enterUserName()
        .enterPassword()
        .clickLoginButton()
        .identifyVendorOption()
        .searchForVendor()
	    .enterVendorId()
	    .clickSearchButton()
	    .getVendorName();

	}
}
