package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DashboardPage extends ProjectMethods {
	
	public DashboardPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="(//button[@class='btn btn-default btn-lg'])[4]") WebElement vendorEle;
	@FindBy(how=How.LINK_TEXT, using="Search for Vendor") WebElement searchButton;
	
	public DashboardPage identifyVendorOption() {
		Actions act = new Actions(driver);
		act.moveToElement(vendorEle).build().perform();
		return this;
	}
	
	public VendorSearchPage searchForVendor() {
		click(searchButton);
		//VendorSearchPage vs = new VendorSearchPage();
		return new VendorSearchPage();
	}

}
 