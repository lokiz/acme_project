package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LoginPage extends ProjectMethods {
	
	public LoginPage() {
		//apply pagefactory
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="email") WebElement emailId;
	@FindBy(how =How.ID, using="password") WebElement password;
	@FindBy(how=How.ID,using="buttonLogin") WebElement loginButton;
	
	public LoginPage enterUserName() {
		clearAndType(emailId,"lokeshbstest@gmail.com");
		return this;
	}
	
	public  LoginPage enterPassword() {
		clearAndType(password,"69242424");
		return this;
	}
	
	public DashboardPage clickLoginButton() {
		click(loginButton);
		//DashboardPage db = new DashboardPage();
		return new DashboardPage();
	}

}
