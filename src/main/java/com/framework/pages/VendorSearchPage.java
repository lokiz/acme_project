package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorSearchPage extends ProjectMethods {
	
	public VendorSearchPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="vendorTaxID") WebElement vendor_TaxIdEle;
	@FindBy(how=How.ID, using="buttonSearch") WebElement searchButton;
	
	public VendorSearchPage enterVendorId() {
		clearAndType(vendor_TaxIdEle,"FR065748");
		return this;
	}
	
	public VendorSearchResults clickSearchButton() {
		click(searchButton);
		return new VendorSearchResults();
	}

}
