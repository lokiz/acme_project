package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorSearchResults extends ProjectMethods{

	public VendorSearchResults() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//table[@class='table']/tbody/tr[2]/td[1]") WebElement vendorName;
	public VendorSearchResults getVendorName() {
	//	String vendorNameText = getElementText(vendorName);
		verifyExactText(vendorName,"Softix Providers");
		return this;
	}
	
	
	
}
